package com.gmail.jsiebert9.redditnotify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * Reddit Notify application class.
 * 
 * @author jsiebert9
 */
@SpringBootApplication
@EnableScheduling
public class Application
{
    /**
     * Application entry point.
     * 
     * @param args starting arguments
     */
    public static void main(String[] args)
    {
        SpringApplication.run(Application.class, args);
    }

    /**
     * Dynamically package name.
     * @return package name
     */
    public String getPackageName()
    {
        return getClass().getPackage().getName();
    }

    /**
     * Get application package name.
     * @return the root app package
     */
    public static String getAppPackage()
    {
        Application app = new Application();
        return app.getPackageName();
    }
}
