package com.gmail.jsiebert9.redditnotify.component;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeMessage;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;

import com.gmail.jsiebert9.redditnotify.config.MailerProperties;
import com.gmail.jsiebert9.redditnotify.config.MailerProperties.SMTPProperties;
import com.gmail.jsiebert9.redditnotify.domain.SubredditConfig;
import com.gmail.jsiebert9.redditnotify.domain.SubredditSubmission;

/**
 * Sends email messages.
 * 
 * @author jsiebert9
 */
@Component
public class Mailer
{
    private final MailerProperties mailProps;
    private final Session mailSession;

    /**
     * Creates a mail session based on mailer properties.
     * 
     * @param props mailer properties
     */
    @Autowired
    public Mailer(MailerProperties props)
    {
        mailProps = props;

        Properties properties = new Properties();
        if (!StringUtils.isBlank(props.getSmtp().getHost()))
        {
            SMTPProperties smtp = props.getSmtp();
            properties.put("mail.smtp.auth", smtp.getAuth());
            properties.put("mail.smtp.host", smtp.getHost());
            properties.put("mail.smtp.port", smtp.getPort());
            properties.put("mail.smtp.user", smtp.getUser());
            properties.put("mail.smtp.starttls.enable", "true");

            mailSession = Session.getDefaultInstance(properties);
        }
        else
        {
            mailSession = Session.getDefaultInstance(properties);
        }
    }

    /**
     * Compose a message.
     *
     * @param config subreddit config
     * @param submission subreddit submission to alert
     * @throws MessagingException failed to send message
     */
    public void compose(SubredditConfig config, SubredditSubmission submission) throws MessagingException
    {
        MimeMessage message = new MimeMessage(mailSession);
        String subject = mailProps.getHeader();
        String body = mailProps.getBody();

        Map<String, String> replaceMap = new HashMap<>();
        replaceMap.put("TITLE", submission.getTitle());
        replaceMap.put("SELFTEXT", submission.getSelftextHtml());
        replaceMap.put("SUBREDDIT", submission.getSubreddit());
        replaceMap.put("SUBREDDIT_PREFIXED", submission.getSubredditNamePrefixed());
        replaceMap.put("AUTHOR", submission.getAuthor());
        replaceMap.put("URL", submission.getUrl());

        // replace variables in subject and body
        for (Map.Entry<String, String> entry : replaceMap.entrySet())
        {
            String value = entry.getValue() != null ? entry.getValue() : "";
            subject = subject.replaceAll("%" + entry.getKey() + "%", value);
            body = body.replaceAll("%" + entry.getKey() + "%", value);
        }

        // set message properties
        MimeMessageHelper helper = new MimeMessageHelper(message, true);
        helper.setFrom(mailProps.getFrom());
        helper.setSubject(subject);
        helper.addTo(config.getRecipient());
        helper.setText(body, true);

        sendMessage(message);
    }

    /**
     * Send a message.
     * 
     * @param message the message to send
     * @throws MessagingException thrown by Transport
     */
    public void sendMessage(Message message) throws MessagingException
    {
        SMTPProperties smtpProps = mailProps.getSmtp();
        if (!StringUtils.isBlank(smtpProps.getHost()))
        {
            // sends the e-mail
            Transport t = mailSession.getTransport("smtp");
            t.connect(smtpProps.getUser(), smtpProps.getPassword());
            t.sendMessage(message, message.getAllRecipients());
            t.close();
        }
        else
        {
            Transport.send(message);
        }
    }
}
