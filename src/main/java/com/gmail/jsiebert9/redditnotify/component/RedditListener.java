package com.gmail.jsiebert9.redditnotify.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.gmail.jsiebert9.redditnotify.domain.Subreddit;
import com.gmail.jsiebert9.redditnotify.domain.SubredditConfig;
import com.gmail.jsiebert9.redditnotify.domain.SubredditListing;
import com.gmail.jsiebert9.redditnotify.domain.SubredditPost;
import com.gmail.jsiebert9.redditnotify.domain.SubredditSubmission;
import com.gmail.jsiebert9.redditnotify.util.JsonUtil;

import lombok.extern.slf4j.Slf4j;

/**
 * Subreddit scraper.
 *
 * @author jsiebert9
 */
@Component
@Slf4j
public class RedditListener
{
    // rate at which to scrape subreddits
    private final int SCRAPE_RATE = 1000 * 60;

    private final Mailer mailer;
    private final RedditService reddit;

    private final Map<Integer, Set<String>> scannedPosts;

    @Autowired
    public RedditListener(Mailer mailer, RedditService reddit)
    {
        this.mailer = mailer;
        this.reddit = reddit;
        scannedPosts = new HashMap<>();
    }

    @Scheduled(fixedRate = SCRAPE_RATE)
    public void scrapeSubreddits()
    {
        log.debug("Invoking Reddit Listener");
        List<SubredditConfig> configList = new ArrayList<>();
        try
        {
            configList = JsonUtil.getSubredditConfig();
        }
        catch (IOException e)
        {
            log.error("Failed to get list of subreddit configurations!", e);
        }

        log.debug("Loaded {} subreddit configurations", configList.size());

        // reddit scrape loop
        for (SubredditConfig subredditConfig : configList)
        {
            String subreddit = subredditConfig.getSubreddit();
            // skip empty subreddit names
            if (StringUtils.isBlank(subreddit))
                continue;

            // initialize the scanned post list
            scannedPosts.computeIfAbsent(subredditConfig.getId(), k -> new HashSet<>());

            // get feed json of new posts
            Subreddit subredditList = reddit.getSubredditPosts(subreddit);
            if (subredditList != null)
            {
                log.debug("Loaded {} posts from r/{}", subredditList.getData().getChildren().size(), subreddit);
                testPosts(subredditConfig, subredditList.getData());
            }
            else
            {
                log.debug("Failed to load posts from r/{}", subreddit);
            }
        }
    }

    private void testPosts(SubredditConfig config, SubredditListing listing)
    {
        List<SubredditPost> posts = listing.getChildren();

        Set<String> scannedPosts = this.scannedPosts.get(config.getId());
        log.debug("scannedPosts: {}", scannedPosts);

        for (SubredditPost post : posts)
        {
            SubredditSubmission submission = post.getData();
            log.debug("Current postId: {}", submission.getId());
            // check if this post was scanned previously
            if (scannedPosts.contains(submission.getId()))
            {
                // skip scanning of this post
                continue;
            }
            scannedPosts.add(submission.getId());

            // only look at self posts?
            if (config.isSelfpostOnly() && !submission.isSelf())
            {
                // skip this submission if not selfpost
                continue;
            }

            // run regex tests on titles and selftexts
            int textMatch = 0;
            int titleMatch = 0;

            // search the title?
            if (config.isSearchTitle())
            {
                titleMatch = matchRegex(submission.getTitle(), config.getKeywords());
            }
            // search the selftext
            if (!StringUtils.isBlank(submission.getSelftext()))
            {
                textMatch = matchRegex(submission.getTitle(), config.getKeywords());
            }

            // if match count hit, trigger notification
            if (textMatch + titleMatch >= config.getKeywordsRequired())
            {
                log.info("[{}] Found a post match: {}", submission.getSubreddit(), submission.getTitle());

                try
                {
                    mailer.compose(config, submission);
                }
                catch (MessagingException e)
                {
                    log.error("Failed to compose notification", e);
                }
            }
        }
    }

    private int matchRegex(String haystack, List<String> needles)
    {
        Pattern pattern;
        Matcher matcher;
        int count = 0;

        for (String needle : needles)
        {
            pattern = Pattern.compile(needle, Pattern.CASE_INSENSITIVE);
            matcher = pattern.matcher(haystack);

            // look for needle in haystack
            if (matcher.find())
            {
                log.debug("Matching groups: {}", matcher.groupCount() + 1);
                // only add one to count
                count++;
            }
        }

        return count;
    }
}
