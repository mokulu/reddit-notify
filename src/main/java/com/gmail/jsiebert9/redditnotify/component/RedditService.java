package com.gmail.jsiebert9.redditnotify.component;

import java.nio.charset.Charset;
import java.util.Base64;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ServerErrorException;

import com.gmail.jsiebert9.redditnotify.Application;
import com.gmail.jsiebert9.redditnotify.config.RedditProperties;
import com.gmail.jsiebert9.redditnotify.domain.AccessToken;
import com.gmail.jsiebert9.redditnotify.domain.Subreddit;

import lombok.extern.slf4j.Slf4j;

/**
 * Reddit API service.
 *
 * @author jsiebert9
 */
@Service
@Slf4j
public class RedditService
{
    private final String appVersion;
    private final RedditProperties redditProps;
    private AccessToken accessToken;

    public RedditService(@Value("${version}") String version, RedditProperties redditProperties)
    {
        appVersion = version;
        redditProps = redditProperties;

        if (StringUtils.isBlank(redditProps.getClientId()))
        {
            throw new RuntimeException("reddit.client-id cannot be blank");
        }
        if (StringUtils.isBlank(redditProps.getClientSecret()))
        {
            throw new RuntimeException("reddit.client-secret cannot be blank");
        }

        // generate headers immediately on startup to get access token
        getHttpEntity();
    }

    /**
     * Encodes basic auth string for OAuth request.
     *
     * @return encoded auth string
     */
    private String getBasicAuthentication()
    {
        String auth = String.format("%s:%s", redditProps.getClientId(), redditProps.getClientSecret());
        byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(Charset.forName("US-ASCII")));
        return new String(encodedAuth);
    }

    private void getAccessToken(HttpHeaders headers)
    {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<AccessToken> response;
        HttpEntity<String> entity;

        // modify headers
        headers.set("Authorization", String.format("Basic %s", getBasicAuthentication()));
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        // create http entity
        entity = new HttpEntity<>("grant_type=client_credentials", headers);

        // send request
        try
        {
            String REDDIT_AUTH_ACCESS_TOKEN = "https://www.reddit.com/api/v1/access_token";
            response = restTemplate.exchange(REDDIT_AUTH_ACCESS_TOKEN, HttpMethod.POST, entity, AccessToken.class);
        }
        catch (HttpClientErrorException e)
        {
            log.error("Failed to get Reddit access token");
            throw e;
        }

        accessToken = response.getBody();
        log.debug("Reddit Access Token received: {}", accessToken);
    }

    /**
     * Generate headers for a request.
     *
     * @return http entity for a reddit request
     */
    private HttpEntity<String> getHttpEntity()
    {
        HttpHeaders headers = new HttpHeaders();
        HttpEntity<String> entity;

        // user agent variables
        String osName = System.getProperty("os.name");
        String appPackage = Application.getAppPackage();

        // set user agent
        headers.set("User-Agent", String.format("%s:%s:%s (by /u/Mokulu)", osName, appPackage, appVersion));

        // authorization is missing or expired, fix it
        if (accessToken == null || accessToken.isExpired())
        {
            getAccessToken(headers);
        }

        // make sure access token is valid
        if (accessToken == null || StringUtils.isBlank(accessToken.getAccessToken()))
        {
            throw new IllegalStateException("Unable to complete request, invalid access token");
        }

        // set authorization
        headers.set("Authorization", String.format("%s %s", accessToken.getTokenType(), accessToken.getAccessToken()));

        entity = new HttpEntity<>("", headers);

        return entity;
    }

    Subreddit getSubredditPosts(String subreddit)
    {
        if (StringUtils.isBlank(subreddit))
        {
            throw new IllegalArgumentException("Subreddit must have a value");
        }

        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<String> entity = getHttpEntity();
        ResponseEntity<Subreddit> response;

        try
        {
            String SUBREDDIT_NEW_POST_URL = String.format("https://oauth.reddit.com/r/%s/new.json", subreddit);
            response = restTemplate.exchange(SUBREDDIT_NEW_POST_URL, HttpMethod.GET, entity, Subreddit.class);
        }
        catch (HttpClientErrorException | ServerErrorException e)
        {
            log.error("Error fetching latest subreddit posts: {}", e.getMessage());
            log.debug(e.getMessage(), e);
            return null;
        }

        return response.getBody();
    }
}
