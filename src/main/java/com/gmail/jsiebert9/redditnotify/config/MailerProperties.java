package com.gmail.jsiebert9.redditnotify.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * Mailer configuration.
 * @author jsiebert9
 */
@Configuration
@ConfigurationProperties("mailer")
@Data
public class MailerProperties
{
    @Data
    public class SMTPProperties
    {
        private String auth;
        private String host;
        private String port;
        private String user;
        private String password;
    }

    private String from;
    private String header;
    private String body;
    private SMTPProperties smtp = new SMTPProperties();
}
