package com.gmail.jsiebert9.redditnotify.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Data;

/**
 * Reddit properties.
 * @author jsiebert9
 */
@Configuration
@ConfigurationProperties("reddit")
@Data
public class RedditProperties
{
    private String clientId;
    private String clientSecret;
}
