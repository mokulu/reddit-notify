package com.gmail.jsiebert9.redditnotify.domain;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Reddit OAuth access token.
 * @author jsiebert9
 */
@Getter
@ToString
@EqualsAndHashCode
public class AccessToken
{
    @JsonProperty("access_token")
    private String accessToken;
    @JsonProperty("token_type")
    private String tokenType;
    @JsonProperty("expires_in")
    private int expiresIn;
    private String scope;
    private Date createDate;

    /**
     * Create
     */
    public AccessToken()
    {
        // set created to now
        createDate = new Date();
    }

    /**
     * Check if access token has expired.
     * @return if is expired
     */
    public boolean isExpired()
    {
        return System.currentTimeMillis() - createDate.getTime() > (expiresIn * 1000);
    }
}
