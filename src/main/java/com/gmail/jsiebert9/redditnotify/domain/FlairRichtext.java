package com.gmail.jsiebert9.redditnotify.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Flair richtext object;
 * @author jsiebert9
 */
@Getter
@ToString
@EqualsAndHashCode
class FlairRichtext
{
    private String e;
    private String t;
}
