package com.gmail.jsiebert9.redditnotify.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Media source object.
 * @author jsiebert9
 */
@Getter
@ToString
@EqualsAndHashCode
class MediaSource
{
    private String url;
    private int width;
    private int height;
}
