package com.gmail.jsiebert9.redditnotify.domain;

import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Post image object.
 * @author jsiebert9
 */
@Getter
@ToString
@EqualsAndHashCode
class PostImage
{
    private MediaSource source;
    private List<MediaSource> resolutions;
    private Object variants;
    private String id;
}
