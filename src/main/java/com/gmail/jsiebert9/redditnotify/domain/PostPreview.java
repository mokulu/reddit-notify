package com.gmail.jsiebert9.redditnotify.domain;

import java.util.List;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Post preview object;
 * @author jsiebert9
 */
@Getter
@ToString
@EqualsAndHashCode
class PostPreview
{
    private List<PostImage> images;
    private boolean enabled;
}
