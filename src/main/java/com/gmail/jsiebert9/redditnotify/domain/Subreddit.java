package com.gmail.jsiebert9.redditnotify.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Subreddit object.
 * @author jsiebert9
 */
@Getter
@ToString
@EqualsAndHashCode
public class Subreddit
{
    private String kind;
    private SubredditListing data;
}
