package com.gmail.jsiebert9.redditnotify.domain;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
@EqualsAndHashCode
public class SubredditConfig
{
    private Integer id;
    private String subreddit = "";
    private List<String> keywords = new LinkedList<>();
    @JsonProperty("keywords_required")
    private Integer keywordsRequired = 1;
    @JsonProperty("search_title")
    private boolean searchTitle = false;
    @JsonProperty("selfpost_only")
    private boolean selfpostOnly = false;
    private String recipient = "someone@example.com";

    public void setId(int id)
    {
        if (this.id == null)
        {
            this.id = id;
        }
        else
        {
            throw new IllegalArgumentException("Id can only be set once");
        }
    }
}
