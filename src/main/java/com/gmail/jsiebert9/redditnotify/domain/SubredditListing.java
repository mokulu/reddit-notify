package com.gmail.jsiebert9.redditnotify.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Subreddit listing object.
 * @author jsiebert9
 */
@Getter
@ToString
@EqualsAndHashCode
public class SubredditListing
{
    private String after;
    private String before;
    private List<SubredditPost> children;
    private int dist;
    private String modhash;
    @JsonProperty("whitelist_status")
    private String whitelistStatus;
}
