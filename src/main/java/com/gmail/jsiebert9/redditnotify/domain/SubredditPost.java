package com.gmail.jsiebert9.redditnotify.domain;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Subreddit post object.
 * @author jsiebert9
 */
@Getter
@ToString
@EqualsAndHashCode
public class SubredditPost
{
    private String kind;
    private SubredditSubmission data;
}
