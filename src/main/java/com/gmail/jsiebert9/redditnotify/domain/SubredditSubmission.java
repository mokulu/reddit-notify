package com.gmail.jsiebert9.redditnotify.domain;

import java.util.LinkedList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * Subreddit submission object.
 * @author jsiebert9
 */
@Getter
@ToString
@EqualsAndHashCode
public class SubredditSubmission
{
    @JsonProperty("approved_at_utc")
    private Long approvedAtUTC;
    @JsonProperty("approved_by")
    private String approvedBy;
    private boolean archived;
    private String author;
    @JsonProperty("author_flair_background_color")
    private String authorFlairBackgroundColor;
    @JsonProperty("author_flair_css_class")
    private String authorFlairCSSClass;
    @JsonProperty("author_flair_richtext")
    private List<FlairRichtext> authorFlairRichtext = new LinkedList<>();
    @JsonProperty("author_flair_text")
    private String authorFlairText;
    @JsonProperty("author_flair_text_color")
    private String authorFlairTextColor;
    @JsonProperty("author_flair_type")
    private String authorFlairType;
    @JsonProperty("banned_at_utc")
    private Long bannedAtUTC;
    @JsonProperty("banned_by")
    private String bannedBy;
    @JsonProperty("brand_safe")
    private boolean brandSafe;
    @JsonProperty("can_gild")
    private boolean canGild;
    @JsonProperty("can_mod_post")
    private boolean canModPost;
    private String category;
    private boolean clicked;
    @JsonProperty("contest_mode")
    private boolean contestMode;
    private Long created;
    @JsonProperty("created_utc")
    private Long createdUTC;
    private String distinguished;
    private String domain;
    private int downs;
    // edited can either be "false" or Long timestamp of when it was edited
    private Object edited;
    private int gilded;
    private boolean hidden;
    @JsonProperty("hide_score")
    private boolean hideScore;
    private String id;
    @JsonProperty("is_crosspostable")
    private boolean crosspostable;
    private boolean is_crosspostable;
    @JsonProperty("is_original_content")
    private boolean originalContent;
    @JsonProperty("is_reddit_media_domain")
    private boolean redditMediaDomain;
    @JsonProperty("is_self")
    private boolean self;
    @JsonProperty("is_video")
    private boolean video;
    private Integer likes;
    @JsonProperty("link_flair_background_color")
    private String linkFlairBackgroundColor;
    @JsonProperty("link_flair_css_class")
    private String linkFlairCSSClass;
    @JsonProperty("link_flair_richtext")
    private List<FlairRichtext> linkFlairRichtext = new LinkedList<>();
    @JsonProperty("link_flair_text")
    private String linkFlairText;
    @JsonProperty("link_flair_text_color")
    private String linkFlairTextColor;
    @JsonProperty("link_flair_type")
    private String linkFlairType;
    private boolean locked;
    private Object media;
    @JsonProperty("media_embed")
    private Object mediaEmbed;
    @JsonProperty("mod_note")
    private String modNote;
    @JsonProperty("mod_reason_by")
    private String modReasonBy;
    @JsonProperty("mod_reason_title")
    private String modReasonTitle;
    @JsonProperty("mod_reports")
    private List<String> modReports;
    private String name;
    @JsonProperty("no_follow")
    private boolean noFollow;
    @JsonProperty("num_comments")
    private Integer numComments;
    @JsonProperty("num_crossposts")
    private Integer numCrossposts;
    @JsonProperty("num_reports")
    private Integer numReports;
    @JsonProperty("over_18")
    private boolean over18;
    @JsonProperty("parent_whitelist_status")
    private String parentWhitelistStatus;
    private String permalink;
    private boolean pinned;
    @JsonProperty("post_hint")
    private String postHint;
    private PostPreview preview;
    private boolean quarantine;
    @JsonProperty("removal_reason")
    private String removalReason;
    // TODO: data type
    @JsonProperty("report_reasons")
    private Object reportReasons;
    @JsonProperty("rte_mode")
    private String rteMode;
    private boolean saved;
    private Integer score;
    // TODO: data type
    private Object secureMedia;
    // TODO: data type
    private List<Object> secureMediaEmbed = new LinkedList<>();
    private String selftext;
    @JsonProperty("selftext_html")
    private String selftextHtml;
    @JsonProperty("send_replies")
    private boolean sendReplies;
    private boolean spoiler;
    private boolean stickied;
    private String subreddit;
    @JsonProperty("subreddit_id")
    private String subredditId;
    @JsonProperty("subreddit_name_prefixed")
    private String subredditNamePrefixed;
    @JsonProperty("subreddit_subscribers")
    private Long subredditSubscribers;
    @JsonProperty("subreddit_type")
    private String subredditType;
    // TODO: data type
    @JsonProperty("suggested_sort")
    private Object suggestedSort;
    private String thumbnail;
    @JsonProperty("thumbnail_height")
    private Integer thumbnailHeight;
    @JsonProperty("thumbnail_width")
    private Integer thumbnailWidth;
    private String title;
    private Integer ups;
    private String url;
    // TODO: data type
    private List<Object> userReports = new LinkedList<>();
    @JsonProperty("view_count")
    private Long viewCount;
    private boolean visited;
    @JsonProperty("whitelist_status")
    private String whitelistStatus;
}
