package com.gmail.jsiebert9.redditnotify.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gmail.jsiebert9.redditnotify.domain.SubredditConfig;

import lombok.extern.slf4j.Slf4j;

/**
 * Json utility class.
 */
@Slf4j
public class JsonUtil
{
    private static final String SUBREDDIT_CONFIG = "subreddits.json";

    /**
     * Private constructor.
     */
    private JsonUtil()
    {
    }

    /**
     * Get list of subreddit scape config.
     *
     * @return list of subreddit scrape config
     * @throws IOException thrown if error with reading config file
     */
    public static List<SubredditConfig> getSubredditConfig() throws IOException
    {
        // create the object mapper
        ObjectMapper mapper = new ObjectMapper();
        // get the json file handle
        File subredditsFile = new File(SUBREDDIT_CONFIG);

        List<SubredditConfig> subredditConfigs;

        // validate that it exists
        if (!subredditsFile.exists())
        {
            // create default
            createSubredditConfig();
        }

        subredditConfigs = mapper.readValue(subredditsFile,
                mapper.getTypeFactory().constructCollectionType(List.class, SubredditConfig.class));

        for (int i = 0; i < subredditConfigs.size(); i++)
        {
            subredditConfigs.get(i).setId(i + 1);
        }

        return subredditConfigs;
    }

    /**
     * Create default subreddit config file.
     */
    private static void createSubredditConfig()
    {
        ObjectMapper mapper = new ObjectMapper();
        SubredditConfig config = new SubredditConfig();
        List<SubredditConfig> defaultConfig = new ArrayList<>();
        defaultConfig.add(config);

        try
        {
            mapper.writerWithDefaultPrettyPrinter().writeValue(new File(SUBREDDIT_CONFIG), defaultConfig);
        }
        catch (IOException e)
        {
            log.warn("Failed to create default {} file!", SUBREDDIT_CONFIG, e);
        }
    }
}
